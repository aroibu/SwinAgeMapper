import torch
a = torch.rand(1,1,160,192,160)
from SwinAgeMapper import SwinAgeMapper
net = SwinAgeMapper(img_size=(160,192,160), in_channels=1)
b = net(a)



# net = SwinAgeMapper(img_size=(160,192,160), in_channels=1, out_channels=1, depths=(2,2,2,2), feature_size=12, num_heads = (2,4,8,16))
# print(sum(p.numel() for p in net.parameters() if p.requires_grad))

# 629088

# net = SwinAgeMapper(img_size=(160,192,160), in_channels=1, out_channels=1, depths=(1,1,1,1), feature_size=12, num_heads = (2,4,8,16))
# print(sum(p.numel() for p in net.parameters() if p.requires_grad))

# for idx in range(len(b)):
#     print(b[idx].shape, b[idx].flatten().shape)


# depts = (1,1,1,1) 
#         (2,2,2,2), 
#         (2,2,6,2)
# num_heads = (3,6,12,24), 
#             (2,4,8,16)
# feature_size = 3, 
#                 6, 
#                 12, 
#                 24


# net = SwinAgeMapper(img_size=(160,192,160), in_channels=1, depths=(2,2,6,2))
# print(sum(p.numel() for p in net.parameters() if p.requires_grad))

# net = SwinAgeMapper(img_size=(160,192,160), in_channels=1, num_heads = (2,4,8,16))
# print(sum(p.numel() for p in net.parameters() if p.requires_grad))

# net = SwinAgeMapper(img_size=(160,192,160), in_channels=1, feature_size=6)
# print(sum(p.numel() for p in net.parameters() if p.requires_grad))


# net = SwinAgeMapper(img_size=(160,192,160), in_channels=1, depths=(1,1,1,1), feature_size=6)
# print(sum(p.numel() for p in net.parameters() if p.requires_grad))


import argparse
parser=argparse.ArgumentParser()
parser.add_argument('-auto', action='store_true')
args=parser.parse_args()
print(args)
print(not args)